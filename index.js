const readline = require('readline');
const http = require('http');
const fs = require('fs');
const os = require('os');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

let message = "Choose an option:\n1. Read package.json\n" +
    "2. Display OS info\n3. Start HTTP server\nType a number: ";
rl.question(message, (answer) => {
    // process.stdout.write("\u001b[2J\u001b[0;0H");
    if (parseInt(answer) == 1) readPackageJson();
    else if (parseInt(answer) == 2) osSpecs();
    else if (parseInt(answer) == 3) webServer();
    else if (message == "q") done = true;
    else console.log("please enter a number from 1 to 3");
    rl.close();
});

function readPackageJson() {
    fs.readFile('package.json', 'utf8', console.log);
}

function osSpecs() {
    console.log("Getting OS info...");
    console.log("SYSTEM MEMORY: " + (os.totalmem() / Math.pow(1024, 3)).toFixed(2) + " GB");
    console.log("FREE MEMORY: " + (os.freemem() / Math.pow(1024, 3)).toFixed(2) + " GB");
    console.log("CPU CORES: " + os.cpus().length);
    console.log("ARCH: " + os.arch());
    console.log("PLATFORM: " + os.platform());
    console.log("RELEASE: " + os.release());
    console.log("USER: " + os.userInfo().username);
}

function webServer() {
    const hostname = '127.0.0.1';
    const port = 3000;

    const server = http.createServer((req, res) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        res.end('Hello World');
    });

    server.listen(port, hostname, () => {
        console.log(`Server running at http://${hostname}:${port}/`);
    });
}